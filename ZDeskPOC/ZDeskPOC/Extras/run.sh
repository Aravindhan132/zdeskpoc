#!/bin/bash

projectname="."
projectfilepath=""
SOURCE="proj"
LANG="objc"
PREFIX="ZA"
HELP=0
while [ $# -gt 0 ]; do
case "$1" in
--help)
HELP=1
;;
--use-swift)
LANG="swift"
;;
--prefix=*)
PREFIX="${1#*=}"
;;
--target-name=*)
targetname="${1#*=}"
;;
--project-name=*)
projectname="${1#*=}"
;;
--project-file-path=*)
projectfilepath="${1#*=}"
;;
*)
exit 1
esac
shift
done

if [ $HELP -eq 1 ];then
echo "Options:\n"
echo "    --project-name       Provide the name of the project"
echo "    --target-name        Provide the name of the target"
echo "    --project-file       Provide the path of the xcproject file"
echo "    --prefix             **EventsExtension.* will be prefixed by this value"
echo "    --use-swift          Generate class for swift"
echo "    --help               Show help banner of specified command"
exit 1
fi

getXcodeproj() {

if [ "$projectfilepath" != "" ]; then
echo $projectfilepath;
else
fileToOpen='';

fileToOpen=$(find "." -maxdepth 2 -name *.xcodeproj | grep "${projectname}")

if [ -n "$fileToOpen" ]
then
echo $fileToOpen;
else
echo nil;
fi
fi

}

python /Users/aravind-zt336/Desktop/Demo/ZDeskPOC/ZDeskPOC/Extras/test.py


if [ ! "$targetname" ]; then
exports="$(xcodebuild -project "$(getXcodeproj)" -showBuildSettings | grep "SOURCE_ROOT\|INFOPLIST_FILE\|PODS_ROOT" | sed -e "s/ = /=/g")"
while read -r line; do     export "$line"; done <<< "$exports"

while [ -z ${SOURCE_ROOT} ]
do

echo "SOURCE_ROOT : ${SOURCE_ROOT}"
echo "Source root is not available, wait for a second"
exports="$(xcodebuild -project "$(getXcodeproj)" -showBuildSettings | grep "SOURCE_ROOT\|INFOPLIST_FILE\|PODS_ROOT" | sed -e "s/ = /=/g")"


