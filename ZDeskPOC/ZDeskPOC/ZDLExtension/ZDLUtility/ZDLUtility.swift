//
//  ZDLUtility.swift
//  ZDesklayoutComponents
//
//  Created by aravind-zt336 on 23/02/20.
//  Copyright © 2020 Zoho. All rights reserved.
//

import Foundation
import UIKit
//import ZohoDeskSDK

public struct ZDLUtility {
    public static func getBundle() -> Bundle? {
        var bundle: Bundle?
        if let urlString = Bundle.main.path(forResource:"ZDeskPOC", ofType: "framework", inDirectory: "Frameworks"){
            bundle = (Bundle(url: URL(fileURLWithPath: urlString)))
        }
        return bundle
    }
}


func getController(_ storyBoardName: String, _ identifier: String) -> UIViewController {
    return UIStoryboard(name: storyBoardName, bundle: ZDLUtility.getBundle()).instantiateViewController(withIdentifier: identifier)
}


extension ZDLUtility {
    
    internal static let orgId: String = "4241905"
    internal static let departmentID: String =  "4000000099140"
    
    enum directions {
        case Top, Left, Right, Bottom
    }
    
    static func getSafeAreaInsets(from direction: directions) -> CGFloat {
        var windowFrame: UIEdgeInsets = UIEdgeInsets.zero
        if #available(iOS 11.0, *) {
            windowFrame = UIApplication.shared.keyWindow?.safeAreaInsets ?? .zero
        }
        switch direction {
        case .Top:
            return windowFrame.top
        case .Bottom:
            return windowFrame.bottom
        default:
            return 0
        }
    }
    
    static func makeSelectionHaptic() {
        if #available(iOS 10.0, *) {
            let hapticGenerator = UISelectionFeedbackGenerator()
            hapticGenerator.selectionChanged()
        }
    }
    
    static func getBottomBarHeight() -> CGFloat {
        let safeAreaHeight: CGFloat = ZDLUtility.getSafeAreaInsets(from: .Bottom)
        let totalBottomBarHeight = 50 + safeAreaHeight
        
        return totalBottomBarHeight
    }
    
    
//    static func getTicketValue(for key: String , ticket: ZDLDataParentModel?) -> String? {
//        
//        let c = ticket?.zdlchilds?.first(where: { (child) -> Bool in
//            let c = child as? ZDLDataChildModel
//            return c?.field_name == key
//        }) as? ZDLDataChildModel
//        return c?.field_value
//        
//    }
    
}

struct ZDTicketKeys {
    static let departmentId = "departmentId"
}

