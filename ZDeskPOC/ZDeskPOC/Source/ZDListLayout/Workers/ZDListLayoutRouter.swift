//
//  ZDListLayoutRouter.swift
//  ZDeskPOC
//
//  Created by aravind-zt336 on 09/03/20.
//  Copyright (c) 2020 Zoho. All rights reserved.
//  Modify By:  * Aravindhan
//              * aravindhan.natarajan@zohocorp.com

import UIKit

protocol IZDListLayoutRouter: class {
	// do someting...
}

class ZDListLayoutRouter: IZDListLayoutRouter {
   
    
	weak var view: ZDListLayoutViewController?
	
	init(view: ZDListLayoutViewController?) {
		self.view = view
	}
  
}
