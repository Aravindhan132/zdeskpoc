//
//  Navigation+CoreDataProperties.swift
//  
//
//  Created by Rathish Marthandan T on 12/03/20.
//
//

import Foundation
import CoreData


extension Navigation {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Navigation> {
        return NSFetchRequest<Navigation>(entityName: "Navigation")
    }

    @NSManaged public var navigateTo: String?
    @NSManaged public var transition: String?

}
