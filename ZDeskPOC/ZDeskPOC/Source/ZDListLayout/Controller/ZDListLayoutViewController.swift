//
//  ZDListLayoutViewController.swift
//  ZDeskPOC
//
//  Created by aravind-zt336 on 09/03/20.
//  Copyright (c) 2020 Zoho. All rights reserved.
//  Modify By:  * Aravindhan
//              * aravindhan.natarajan@zohocorp.com

import UIKit

protocol IZDListLayoutViewController: class {
	var router: IZDListLayoutRouter? { get set }
    func presentLayout(form: FormView?, fields: [Field] , patternType: String?)
}



public class ZDListLayoutViewController: UIViewController , UIGestureRecognizerDelegate , ZDListProtocol {
    
    internal var interactor: IZDListLayoutInteractor?
	internal var router: IZDListLayoutRouter?
    
    internal var listDelegate: ZDListLayoutDelegate? = nil
    internal var listDataSource: ZDListLayoutDataSource? = nil
    internal var listLayoutApi: ZDListProtocol? = nil
    internal var eventHandler: ZDEventHandler? = nil
    
    @IBOutlet var listTableView: UITableView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var nodataLabel: UILabel!
    
    
    internal  lazy var field_Keys: [String] = []
    internal  lazy var layoutType: String? = nil
    internal lazy var screenName: String = ""
    var sourceIndex: IndexPath?
    internal lazy var isLoadMoreEnabled: Bool = true
    internal lazy var parentData: Any? = nil
    
    lazy var uiRefreshController: UIRefreshControl = {
           let refreshController = UIRefreshControl()
           refreshController.addTarget(self, action: #selector(self.refreshLoadedData), for: .valueChanged)
           return refreshController
       }()
    
    override public func viewDidLoad() {
        super.viewDidLoad()
		// do someting...
        
        self.prepareEventHander()
        self.prepareViewRequirements()
        
        self.interactor?.getListLayout(screenTitle: screenName)
    }
    
}

extension ZDListLayoutViewController: IZDListLayoutViewController {
    // do someting...
    
    func presentLayout(form: FormView? , fields: [Field], patternType: String?) {
        self.layoutType = patternType
        self.field_Keys = fields.map({($0.fieldName ?? "")})
        self.listDataSource?.prepareInitialDataSource(layout: .list, screenTitle: screenName, parentDataModel: parentData, {
            self.prepareInitialDataSourceCompletion()
        })
    }
    
    
}

extension ZDListLayoutViewController : UITableViewDelegate , UITableViewDataSource{
    // do someting...
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listDataSource?.getTotalItems(layout: .list, screenTitle: screenName) ?? .zero
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  self.prepareCellPattern(for: indexPath)
        return cell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) else {return}
        
        let form = CoreDataHelper.fetchLayout(ofType: appletType.list.rawValue, ofTitle: (self.screenName))
        
        guard let actionSet = ((form?.pattern?.allObjects)?.first as? FormFieldPattern)?.action ,
            let tapaction = (Array<Any>(actionSet) as? [Action])?.filter({$0.uiAction == fieldActions.tap.rawValue}).first else {return}
        
        cell.transformAnimation(duration: 0.1, bounceLength: 0.9) {
            self.prepareEventActions(indexpath: indexPath, swipeTitle: fieldActions.tap.rawValue, action: tapaction)
        }
    }
    
}

@available(iOS 11.0, *)
extension ZDListLayoutViewController {
    public func tableView(_ tableView: UITableView,
                          leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
     
      let form = CoreDataHelper.fetchLayout(ofType: appletType.list.rawValue, ofTitle: (self.screenName))

       guard let nonNilForm = form  ,
             let actionSet = ((nonNilForm.pattern?.allObjects)?.first as? FormFieldPattern)?.action ,
             let actions = Array<Any>(actionSet) as? [Action]
             else { return nil }
       
        var configactions: [UIContextualAction] = []
        
        actions.filter({$0.uiAction == fieldActions.leftswipe.rawValue}).forEach { (eachLeftActions) in
            let title = eachLeftActions.perform ?? ""
            let action = UIContextualAction(style: .normal ,
                                                 title:  title,
                                                 handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
                self.prepareEventActions(indexpath: indexPath, swipeTitle: title , action: eachLeftActions)
                success(true)
            })
            action.backgroundColor  = getRandomColor()
            configactions.append(action)
        }

       return UISwipeActionsConfiguration(actions: configactions)
    }
    
    public func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
                
        let form = CoreDataHelper.fetchLayout(ofType: appletType.list.rawValue, ofTitle: (self.screenName))

        guard let nonNilForm = form  ,
              let actionSet = ((nonNilForm.pattern?.allObjects)?.first as? FormFieldPattern)?.action ,
              let actions = Array<Any>(actionSet) as? [Action] ,
              let rightaction = actions.filter({$0.uiAction == fieldActions.rightswift.rawValue}).first else {return nil}
        
        
        let swipeTitle = rightaction.perform ?? ""

        let closeAction = UIContextualAction(style: .normal ,
                                             title:  swipeTitle,
                                             handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            self.prepareEventActions(indexpath: indexPath, swipeTitle: swipeTitle , action: rightaction)
            success(true)
        })
        
        closeAction.backgroundColor = getRandomColor()
        return UISwipeActionsConfiguration(actions: [closeAction])
    }
}

extension ZDListLayoutViewController {
    
    func prepareEventActions(indexpath: IndexPath , swipeTitle: String , action: Action?) {
        self.startAnimation()
        self.sourceIndex = indexpath
        
        self.eventHandler?.triggerAction(for: action,
                                         selectedIndex: indexpath,
                                         swipeTitle: swipeTitle,
                                         sourceIndex: self.listLayoutApi?.getParentIndex(),
                                         screenTitle: self.title ?? "")
    }
    
    
}


extension ZDListLayoutViewController: ZDListEventsProtocol {
  
    
    func preActionCompletion(for action: Action?, selectedIndex: IndexPath, screenTitle: String, haveCallback: Bool , navigationModel: Navigation?) {
        self.stopAnimation()
        self.refreshPage()
        self.listLayoutApi?.refreshPage()
    }
    
    func preparePostActionCompletion(for action: Action?, selectedIndex: IndexPath, screenTitle: String , parentModel: Any?, navigationModel: Navigation?) {

    }
    
    
}


//MARK:- Util Works Goes Here
extension ZDListLayoutViewController {
    
    fileprivate func prepareViewRequirements() -> Void {
        self.startAnimation()
        self.nodataLabel.isHidden = true
        self.listTableView.addSubview(uiRefreshController)
        self.prepareNavigateBar()
        self.prepareTableView()
    }
    
    fileprivate func prepareInitialDataSourceCompletion() {
        self.stopAnimation()
        DispatchQueue.main.async {
            if self.listDataSource?.getTotalItems(layout: .list, screenTitle: self.screenName) ?? .zero == .zero {
                self.nodataLabel.isHidden = false
            }
            self.listTableView.reloadData()
            self.listTableView.separatorStyle = .singleLine
            self.uiRefreshController.endRefreshing()
        }
    }
    
    @objc internal func refreshLoadedData() -> Void{
        DispatchQueue.main.async {
            self.interactor?.getListLayout(screenTitle: self.screenName)
        }
    }
    
    func prepareTableView() {
        self.listTableView.delegate = self
        self.listTableView.dataSource = self
        self.listTableView.tableFooterView = UIView(frame: CGRect.zero)
        self.listTableView.contentInset = .zero
        self.listTableView.separatorStyle = .none
        self.listTableView.rowHeight = UITableView.automaticDimension
        self.listTableView.register(UINib(nibName: "ZDListPattern1", bundle: ZDLUtility.getBundle()), forCellReuseIdentifier: "ZDListPattern1")
        self.listTableView.register(UINib(nibName: "ZDListPattern2", bundle: ZDLUtility.getBundle()), forCellReuseIdentifier: "ZDListPattern2")
        self.listTableView.register(UINib(nibName: "ZDListPattern3", bundle: ZDLUtility.getBundle()), forCellReuseIdentifier: "ZDListPattern3")
        self.listTableView.register(UINib(nibName: "ZDListPattern4", bundle: ZDLUtility.getBundle()), forCellReuseIdentifier: "ZDListPattern4")
        self.listTableView.register(UINib(nibName: "ZDListPattern5", bundle: ZDLUtility.getBundle()), forCellReuseIdentifier: "ZDListPattern5")
    }
    
    
    fileprivate func prepareEventHander() {
        self.eventHandler = ZDEventHandler()
        self.eventHandler?.eventProtocol = self
        self.eventHandler?.listDelegate = self.listDelegate
    }
    
    fileprivate func prepareNavigateBar() {
        self.title = self.screenName
        self.navigationController?.title = self.screenName
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
    }
    
    func refreshPage() {
        DispatchQueue.main.async {
            self.listTableView.reloadData()
        }
    }
    
    func getParentIndex() -> IndexPath? {
        return self.sourceIndex
    }
}
