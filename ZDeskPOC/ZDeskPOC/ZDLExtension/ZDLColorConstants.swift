//
//  ZDLColorConstants.swift
//  ZDesklayoutComponents
//
//  Created by aravind-zt336 on 23/02/20.
//
 
import Foundation
import UIKit

class ZDColorConstants {
    
    static let primaryDark = #colorLiteral(red: 0.1411764706, green: 0.1411764706, blue: 0.1738255019, alpha: 1)
    static let secondaryDark = #colorLiteral(red: 0.1411764706, green: 0.1411764706, blue: 0.1568627451, alpha: 1)
    
    
    static let primaryLight = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    static let secondaryLight = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    
    
    static let Background_primary          = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    static let Background_secondary        = #colorLiteral(red: 0.9568627451, green: 0.9607843137, blue: 0.9725490196, alpha: 1)
    static let Background_teritary        = #colorLiteral(red: 0.8962672353, green: 0.8894707561, blue: 0.9206243157, alpha: 1)
    
    static let white = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    static let gray = #colorLiteral(red: 0.5898534656, green: 0.5925863981, blue: 0.5992555022, alpha: 1)
    static let appIconcolor = #colorLiteral(red: 0.3725490196, green: 0.4196078431, blue: 0.8745098039, alpha: 1)
    static let primaryBlue = #colorLiteral(red: 0, green: 0.2, blue: 0.4, alpha: 1)
    static let primaryGreen = #colorLiteral(red: 0, green: 0.6793572307, blue: 0.3375645876, alpha: 0.1964362158)
    
    static let textprimaryGreen = #colorLiteral(red: 0, green: 0.6793572307, blue: 0.3375645876, alpha: 1)
    
    
    static let separatorColor = UIColor.black.withAlphaComponent(0.1)
    
    struct BottomBar {
        static let backgroundColor: UIColor = #colorLiteral(red: 0.1411764706, green: 0.1411764706, blue: 0.1738255019, alpha: 1)
        static let selectedColor: UIColor = #colorLiteral(red: 0.1411764706, green: 0.1411764706, blue: 0.1738255019, alpha: 1)
        static let unSelectedColor: UIColor = gray
    }
    
    
    struct Background {
        static let primary: UIColor = primaryLight
        static let secondary: UIColor = #colorLiteral(red: 0.8498341441, green: 0.8447830081, blue: 0.8537173867, alpha: 1)

    }
    
    
    struct Text {
        static let primary: UIColor = UIColor.black
        static let secondary: UIColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
    }
}

func getRandomColor() -> UIColor {
     //Generate between 0 to 1
     let red:CGFloat = CGFloat(drand48())
     let green:CGFloat = CGFloat(drand48())
     let blue:CGFloat = CGFloat(drand48())

     return UIColor(red:red, green: green, blue: blue, alpha: 1.0)
}
