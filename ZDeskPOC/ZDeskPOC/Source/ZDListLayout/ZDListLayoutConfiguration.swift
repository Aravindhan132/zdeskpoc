//
//  ZDListLayoutConfiguration.swift
//  ZDeskPOC
//
//  Created by aravind-zt336 on 09/03/20.
//  Copyright (c) 2020 Zoho. All rights reserved.
//  Modify By:  * Aravindhan
//              * aravindhan.natarajan@zohocorp.com

import Foundation
import UIKit

public class ZDListLayoutConfiguration {
    public static func setup(parameters: [String: Any] = [:]) -> ZDListLayoutViewController {
            let controller = getController("ZDList", "ZDListLayoutViewController") as! ZDListLayoutViewController
            let router = ZDListLayoutRouter(view: controller)
            let presenter = ZDListLayoutPresenter(view: controller)
            let manager = ZDListLayoutManager()
            let interactor = ZDListLayoutInteractor(presenter: presenter, manager: manager)
            
            controller.interactor = interactor
            controller.router = router
            interactor.parameters = parameters
            return controller        
    }
}
