//
//  ZDLayoutNavigator.swift
//  ZDeskPOC
//
//  Created by aravind-zt336 on 17/03/20.
//  Copyright © 2020 Zoho. All rights reserved.
//

import UIKit

enum presentationSytle: String {
    case push = "push"
    case present = "present"
    case dismiss = "dismiss"
    case pop = "pop"
}

class ZDLayoutNavigator: NSObject {

    static func moveToNextController(form: Navigation, parentData: Any? , view: UIViewController? , isGrid: Bool , _ onCompletion: (() -> Void)? = nil) {
        DispatchQueue.main.async {
            guard let toScreenName = form.navigateTo , let transition = form.transition else {return}
            switch transition {
            case presentationSytle.push.rawValue:
                self.pushToFormLayout(isGrid: isGrid, title: toScreenName, parentData: parentData, view: view); onCompletion?()
            case presentationSytle.present.rawValue:
                self.presentToFormLayout(isGrid: isGrid, title: toScreenName, parentData: parentData, view: view) { onCompletion?() }
            case presentationSytle.dismiss.rawValue:
                view?.dismiss(animated: true, completion: { onCompletion?() })
            case presentationSytle.pop.rawValue:
                view?.navigationController?.popViewController(animated: true); onCompletion?()
            default: break
            }
        }
    }
    
    static fileprivate func pushToFormLayout(isGrid: Bool , title: String, parentData: Any?, view: UIViewController?) {
        let initalize = ZDeskLayoutSDK.init(controller: nil)
        initalize.screenName = title
        
        if isGrid {
            let controller = initalize.preapreGridFormLayout()
            controller.parentData = parentData
            view?.navigationController?.pushViewController(controller, animated: true)
        } else {
            let controller = initalize.prepareFormLayout()
            controller.parentData = parentData
            controller.listLayoutApi = view as? ZDListProtocol
            view?.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    static fileprivate func presentToFormLayout(isGrid: Bool , title: String , parentData: Any?, view: UIViewController? , _ onCompletion: (() -> Void)? = nil) {
        let initalize = ZDeskLayoutSDK.init(controller: nil)
        initalize.screenName = title
        
        if isGrid {
            let controller = initalize.preapreGridFormLayout()
            controller.parentData = parentData
            view?.present(controller, animated: true, completion: {
                onCompletion?()
            })
            
        } else {
            let controller = initalize.prepareFormLayout()
            controller.listLayoutApi = view as? ZDListProtocol
            controller.parentData = parentData
            view?.present(UINavigationController(rootViewController: controller), animated: true, completion: {
                onCompletion?()
            })
        }
    }
    
    
}
