//
//  ZDListGridLayoutViewController.swift
//  ZDeskPOC
//
//  Created by aravind-zt336 on 09/03/20.
//  Copyright (c) 2020 Zoho. All rights reserved.
//  Modify By:  * Aravindhan
//              * aravindhan.natarajan@zohocorp.com

import UIKit

protocol IZDListGridLayoutViewController: class {
    var router: IZDListGridLayoutRouter? { get set }
    func presentLayout(form: FormView?)
}

public class ZDListGridLayoutViewController: UIViewController {
    var interactor: IZDListGridLayoutInteractor?
    var router: IZDListGridLayoutRouter?
    
    
    @IBOutlet var listCollectionView: UICollectionView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    public var listDelegate: ZDListLayoutDelegate? = nil
    public var listDataSource: ZDListLayoutDataSource? = nil
    internal var eventHandler: ZDEventHandler? = nil

    
    var field_Keys: [String] = []
    var dataModel: [[ZDListDataItem]]? = []
    var layoutType: String? = nil
    
    var estimateWidth = 160.0
    var cellMarginSize = 16.0
    
    public var screenName: String = ""
    var parentData: Any?  = nil
    private var form: FormView? = nil {
        didSet {
            print("setted")
        }
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        // do someting...
        
        prepareEventHander()
        startAnimation()
        interactor?.getListLayout(screenTitle: screenName)
        
        prepareNavigateBar()
        prepareCollectionView()
    }
    
    fileprivate func prepareEventHander() {
           self.eventHandler = ZDEventHandler()
           self.eventHandler?.eventProtocol = self
           self.eventHandler?.listDelegate = self.listDelegate
       }
    
    fileprivate func prepareNavigateBar() {
        self.title = self.screenName
        self.navigationController?.title = self.screenName
    }
    
    
    fileprivate func registerCollectionViewCells() -> Void {
        self.listCollectionView.register(UINib(nibName: "ZDViewGridCell", bundle: ZDLUtility.getBundle()), forCellWithReuseIdentifier: "ZDViewGridCell")
        self.listCollectionView.register(UINib(nibName: "ZDQuickViewListCell", bundle:  ZDLUtility.getBundle()), forCellWithReuseIdentifier: "ZDQuickViewListCell")
        
        
        self.listCollectionView.register(UINib(nibName: "ZDTitleHeaderCollectionCell", bundle:  ZDLUtility.getBundle()),
                                         forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
                                         withReuseIdentifier: "ZDTitleHeaderCollectionCell")
        
        self.setupGridView()
    }
    
    
    fileprivate func prepareCollectionView() -> Void {
        self.listCollectionView.delegate = self
        self.listCollectionView.dataSource = self
        self.listCollectionView.contentInset =  UIEdgeInsets(top: 0, left: 0, bottom: ZDLUtility.getBottomBarHeight(), right: 0)
        
        self.registerCollectionViewCells()
    }
    
    func setupGridView() {
        let flow = listCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        flow.minimumInteritemSpacing = CGFloat(self.cellMarginSize)
        flow.minimumLineSpacing = CGFloat(self.cellMarginSize)
    }
    
    
    func startAnimation() {
        DispatchQueue.main.async {
            self.activityIndicator.startAnimating()
        }
    }
    
    func stopAnimation() {
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
        }
    }
}

extension ZDListGridLayoutViewController: IZDListGridLayoutViewController {
    // do someting...
    func presentLayout(form: FormView?) {
        
        self.form = form
        field_Keys.removeAll()
        
        
        let pattern = ((form?.pattern? .allObjects)?.first as? FormFieldPattern)?.fields
        
        guard let nonNilPatter = pattern else {return}
        var fields = Array<Any>(nonNilPatter) as? [Field]
        
        
        fields = fields?.sorted { (field1, field2) -> Bool in
            field1.fieldOrder < field2.fieldOrder
        }
        
        
        guard let nonNilFields = fields else {return}
        
        nonNilFields.forEach({
            field_Keys.append($0.fieldName ?? "")
        })
        
        self.listDataSource?.prepareInitialDataSource(layout: .list, screenTitle: self.screenName , parentDataModel: self.parentData, {
            DispatchQueue.main.async {
                self.stopAnimation()
                self.listCollectionView.performBatchUpdates({
                    self.listCollectionView.reloadData()
                }, completion: nil)
            }
        })
    }
}

extension ZDListGridLayoutViewController: UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
    // do someting...
    
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.listDataSource?.getTotalItems(layout: .list, screenTitle: screenName) ?? .zero
    }
    
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return self.prepareGridLayoutCells(collectionView, cellForItemAt: indexPath)
    }
    
    
    fileprivate func prepareGridLayoutCells(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ZDViewGridCell", for: indexPath) as? ZDViewGridCell else {fatalError()}
        self.listDataSource?.listItem(layout: .list, screenTitle: screenName , atIndexPath: indexPath, fromKeys: ZDFormModel.generateDataItem(usingKeys: field_Keys), { (listDataItems) in
            DispatchQueue.main.async {
                cell.setTitle(as: listDataItems?.first?.value ?? "" , secondaryLabel: listDataItems?[1].value ?? "")
            }
        })
        
        
        return cell
    }
    
    fileprivate func prepareListLayoutCell(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ZDQuickViewListCell", for: indexPath) as? ZDQuickViewListCell , let viewModel =  dataModel?.first , let viewName =  viewModel[indexPath.row].value else {fatalError()}
        cell.setTitle(as: viewName)
        return cell
    }
    
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        guard let form = CoreDataHelper.fetchLayout(ofType: appletType.list.rawValue, ofTitle: self.screenName) ,
            let cell = listCollectionView.cellForItem(at: indexPath) ,
            let actionSet = ((form.pattern?.allObjects)?.first as? FormFieldPattern)?.action ,
            let tapaction = (Array<Any>(actionSet) as? [Action])?.filter({$0.uiAction == fieldActions.tap.rawValue}).first else {return}
        
        
        cell.transformAnimation(duration: 0.1, bounceLength: 0.9) {
            self.eventHandler?.triggerAction(for: tapaction,
                                             selectedIndex: indexPath,
                                             swipeTitle: fieldActions.tap.rawValue,
                                             sourceIndex: nil,
                                             screenTitle: self.title ?? "")
        }
        
       
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = self.calculateWith()
        return CGSize(width: width, height: width * 0.7)
    }
    
    func calculateWith() -> CGFloat {
        let estimatedWidth = CGFloat(estimateWidth)
        let cellCount = floor(CGFloat(self.view.frame.size.width / estimatedWidth))
        
        let margin = CGFloat(cellMarginSize * 2)
        let width = (self.view.frame.size.width - CGFloat(cellMarginSize) * (cellCount - 1) - margin) / cellCount
        return width
    }
    
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 16.0, left: 16.0, bottom: 16.0, right: 16.0)
    }
    
}

extension ZDListGridLayoutViewController: ZDListEventsProtocol {
    func preActionCompletion(for action: Action?, selectedIndex: IndexPath, screenTitle: String, haveCallback: Bool, navigationModel: Navigation?) {
        
    }
    
    func preparePostActionCompletion(for action: Action?, selectedIndex: IndexPath, screenTitle: String, parentModel: Any?, navigationModel: Navigation?) {
        
    }
    
}
