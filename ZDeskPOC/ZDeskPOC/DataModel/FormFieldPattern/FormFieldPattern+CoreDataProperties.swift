//
//  FormFieldPattern+CoreDataProperties.swift
//  
//
//  Created by Rathish Marthandan T on 06/03/20.
//
//

import Foundation
import CoreData


extension FormFieldPattern {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<FormFieldPattern> {
        return NSFetchRequest<FormFieldPattern>(entityName: "FormFieldPattern")
    }

    @NSManaged public var fieldPatternName: String?
    @NSManaged public var fields: NSSet?
    @NSManaged public var action: NSSet?

}

// MARK: Generated accessors for fields
extension FormFieldPattern {

    @objc(addFieldsObject:)
    @NSManaged public func addToFields(_ value: Field)

    @objc(removeFieldsObject:)
    @NSManaged public func removeFromFields(_ value: Field)

    @objc(addFields:)
    @NSManaged public func addToFields(_ values: NSSet)

    @objc(removeFields:)
    @NSManaged public func removeFromFields(_ values: NSSet)

}

// MARK: Generated accessors for action
extension FormFieldPattern {

    @objc(addActionObject:)
    @NSManaged public func addToAction(_ value: Action)

    @objc(removeActionObject:)
    @NSManaged public func removeFromAction(_ value: Action)

    @objc(addAction:)
    @NSManaged public func addToAction(_ values: NSSet)

    @objc(removeAction:)
    @NSManaged public func removeFromAction(_ values: NSSet)

}
