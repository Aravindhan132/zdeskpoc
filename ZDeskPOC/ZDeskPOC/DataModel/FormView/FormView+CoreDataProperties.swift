//
//  FormView+CoreDataProperties.swift
//  
//
//  Created by Rathish Marthandan T on 09/03/20.
//
//

import Foundation
import CoreData


extension FormView {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<FormView> {
        return NSFetchRequest<FormView>(entityName: "FormView")
    }

    @NSManaged public var isGrid: Bool
    @NSManaged public var layoutType: String?
    @NSManaged public var title: String?
    @NSManaged public var navigationItems: NSSet?
    @NSManaged public var pattern: NSSet?

}

// MARK: Generated accessors for navigationItems
extension FormView {

    @objc(addNavigationItemsObject:)
    @NSManaged public func addToNavigationItems(_ value: NavigationItem)

    @objc(removeNavigationItemsObject:)
    @NSManaged public func removeFromNavigationItems(_ value: NavigationItem)

    @objc(addNavigationItems:)
    @NSManaged public func addToNavigationItems(_ values: NSSet)

    @objc(removeNavigationItems:)
    @NSManaged public func removeFromNavigationItems(_ values: NSSet)

}

// MARK: Generated accessors for pattern
extension FormView {

    @objc(addPatternObject:)
    @NSManaged public func addToPattern(_ value: FormFieldPattern)

    @objc(removePatternObject:)
    @NSManaged public func removeFromPattern(_ value: FormFieldPattern)

    @objc(addPattern:)
    @NSManaged public func addToPattern(_ values: NSSet)

    @objc(removePattern:)
    @NSManaged public func removeFromPattern(_ values: NSSet)

}
