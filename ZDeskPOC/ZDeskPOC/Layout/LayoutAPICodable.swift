// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let contactStats = try? newJSONDecoder().decode(ContactStats.self, from: jsonData)

import Foundation

// MARK: - ContactStats
public class ContactStats: Codable {
    var data: [Datum]?

    init(data: [Datum]?) {
        self.data = data
    }
}

// MARK: - Datum
class Datum: Codable {
    var id, type: String?
    var info: Info?
    var zplatformTemplates: [ZplatformTemplate]?
    var zplatformComponents: [DatumZplatformComponent]?

    enum CodingKeys: String, CodingKey {
        case id, type, info
        case zplatformTemplates = "zplatform_templates"
        case zplatformComponents = "zplatform_components"
    }

    init(id: String?, type: String?, info: Info?, zplatformTemplates: [ZplatformTemplate]?, zplatformComponents: [DatumZplatformComponent]?) {
        self.id = id
        self.type = type
        self.info = info
        self.zplatformTemplates = zplatformTemplates
        self.zplatformComponents = zplatformComponents
    }
}

// MARK: - Info
class Info: Codable {
    var isGrid: Bool?
    var module: String?

    init(isGrid: Bool?, module: String?) {
        self.isGrid = isGrid
        self.module = module
    }
}

// MARK: - DatumZplatformComponent
class DatumZplatformComponent: Codable {
    var id, fieldName: String?
    var type: String?
    var isIconAvailable, isLabelAvailabke, labelText: String?

    enum CodingKeys: String, CodingKey {
        case id
        case fieldName = "field_name"
        case type, isIconAvailable, isLabelAvailabke, labelText
    }

    init(id: String?, fieldName: String?, type: String?, isIconAvailable: String?, isLabelAvailabke: String?, labelText: String?) {
        self.id = id
        self.fieldName = fieldName
        self.type = type
        self.isIconAvailable = isIconAvailable
        self.isLabelAvailabke = isLabelAvailabke
        self.labelText = labelText
    }
}

// MARK: - ZplatformTemplate
class ZplatformTemplate: Codable {
    var type: String?
    var zplatformAction: [ZplatformAction]?
    var zplatformComponents: [ZplatformTemplateZplatformComponent]?

    enum CodingKeys: String, CodingKey {
        case type
        case zplatformAction = "zplatform_action"
        case zplatformComponents = "zplatform_components"
    }

    init(type: String?, zplatformAction: [ZplatformAction]?, zplatformComponents: [ZplatformTemplateZplatformComponent]?) {
        self.type = type
        self.zplatformAction = zplatformAction
        self.zplatformComponents = zplatformComponents
    }
}

// MARK: - ZplatformAction
class ZplatformAction: Codable {
    var uiAction: String?
    var resultRequired: Bool?
    var type: String?
    var zplatformNavigation: ZplatformNavigation?
    var perform: String?

    enum CodingKeys: String, CodingKey {
        case uiAction, resultRequired, type
        case zplatformNavigation = "zplatform_navigation"
        case perform
    }

    init(uiAction: String?, resultRequired: Bool?, type: String?, zplatformNavigation: ZplatformNavigation?, perform: String?) {
        self.uiAction = uiAction
        self.resultRequired = resultRequired
        self.type = type
        self.zplatformNavigation = zplatformNavigation
        self.perform = perform
    }
}

// MARK: - ZplatformNavigation
class ZplatformNavigation: Codable {
    var destination: String?
    var arguments: [String]?

    init(destination: String?, arguments: [String]?) {
        self.destination = destination
        self.arguments = arguments
    }
}

// MARK: - ZplatformTemplateZplatformComponent
class ZplatformTemplateZplatformComponent: Codable {
    var id, fieldName: String?
    var type: String?
    var isIconAvailable, isLabelAvailable, labelText: String?
    var textSize, labeltextSize: Int?
    var labelColor, textColor: String?
    var fieldBgColor: String?
    var zplatformAction: [ZplatformAction]?

    enum CodingKeys: String, CodingKey {
        case id
        case fieldName = "field_name"
        case type, isIconAvailable, isLabelAvailable, labelText, textSize, labeltextSize, labelColor, textColor, fieldBgColor
        case zplatformAction = "zplatform_action"
    }

    init(id: String?, fieldName: String?, type: String?, isIconAvailable: String?, isLabelAvailable: String?, labelText: String?, textSize: Int?, labeltextSize: Int?, labelColor: String?, textColor: String?, fieldBgColor: String?, zplatformAction: [ZplatformAction]?) {
        self.id = id
        self.fieldName = fieldName
        self.type = type
        self.isIconAvailable = isIconAvailable
        self.isLabelAvailable = isLabelAvailable
        self.labelText = labelText
        self.textSize = textSize
        self.labeltextSize = labeltextSize
        self.labelColor = labelColor
        self.textColor = textColor
        self.fieldBgColor = fieldBgColor
        self.zplatformAction = zplatformAction
    }
}

