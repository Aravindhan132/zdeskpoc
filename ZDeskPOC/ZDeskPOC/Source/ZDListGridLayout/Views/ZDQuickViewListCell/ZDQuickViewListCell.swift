//
//  ZDQuickViewListCell.swift
//  DeskDemo
//
//  Created by aravind-zt336 on 14/02/20.
//  Copyright © 2020 Zoho. All rights reserved.
//

import UIKit

class ZDQuickViewListCell: UICollectionViewCell {

    
    @IBOutlet var titleLable: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.applyFontAndTheme()
    }

    func applyFontAndTheme() -> Void {
        self.layer.cornerRadius = 10
        self.clipsToBounds = true
        
        self.layer.borderWidth = 0.5
        self.layer.borderColor = ZDColorConstants.primaryGreen.cgColor
    }
    
    func setTitle(as name: String) -> Void {
        titleLable.text = name
    }
}
