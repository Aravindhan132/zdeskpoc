//
//  ZDListPattern1.swift
//  ZDeskPOC
//
//  Created by aravind-zt336 on 17/03/20.
//  Copyright © 2020 Zoho. All rights reserved.
//

import UIKit

class ZDListPattern1: UITableViewCell {

    
    @IBOutlet var label1: UILabel!
    @IBOutlet var label2: UILabel!
    @IBOutlet var label3: UILabel!
    @IBOutlet var label4: UILabel!
    @IBOutlet var label5: UILabel!
    
    var labelArray: [UILabel] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
        self.labelArray = [label1 , label2 , label3 , label4 , label5]
        labelArray.forEach({
            $0.text = ""
            $0.backgroundColor = .clear
        })
        self.backgroundColor = .clear
    }

    
    internal func loadData(for values: [ZDListDataItem]?) {
        guard let nonNilValue = values else {return}
        
        labelArray.enumerated().forEach { (index , eachLabel) in
            if index < nonNilValue.count {
                eachLabel.text = nonNilValue[index].value ?? ""
            }
        }
        
    }
    
}
