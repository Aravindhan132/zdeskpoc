//
//  GVHeaderTitleView.swift
//  Gagavox
//
//  Created by aravind-pt2199 on 04/02/20.
//  Copyright © 2020 Gagavox. All rights reserved.
//

import UIKit

class ZDHeaderTitleView: UIView {

    @IBOutlet var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.applyFontAndTheme()
    }
   
    func applyFontAndTheme() {
        self.titleLabel.textColor = ZDColorConstants.Text.primary
        self.backgroundColor = .clear
    }
    
    func setTitle(name: String) {
        self.titleLabel.text = name
    }

}
