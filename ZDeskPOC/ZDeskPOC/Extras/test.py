import os
print("*********************Inside PYTHTON Scipt the File*********************")

save_path = "/Users/aravind-zt336/Desktop/Demo/ZDeskPOC/ZDeskPOC/Extras"#os.getcwd() + "/ZDeskPOCApp"

name_of_file = "ZDScreenList"

completeName = os.path.join(save_path, name_of_file+".swift")         

file1 = open(completeName, "w")

screenList_cases="ZDScreenListCases"
screenList = ["QuickView" , "TicketListView" , "AssigneListView" , "ContactList"]
screenContent = "public enum %s {  case %s , %s , %s } \n" % (screenList_cases , screenList[0], screenList[1] , screenList[2])

ticketList_cases = "ZDTicketListCases"
ticket_Objects = ["ticketNumber" , "status" , "subject" ,"assigneename","contactname"]
content1 = "public enum %s {  case %s , %s , %s , %s , %s }\n" % (ticketList_cases , ticket_Objects[0], ticket_Objects[1],ticket_Objects[2],ticket_Objects[3],ticket_Objects[4])

agentList_cases = "ZDAgentListCases"
agent_Objects = ["name" , "mobile" , "email" ,"status","roleId"]
content2 = "public enum %s {  case %s , %s , %s, %s , %s }\n" % (agentList_cases,agent_Objects[0],agent_Objects[1],agent_Objects[2],agent_Objects[3],agent_Objects[4])

quickViewList_cases = "ZDQuickViewCases"
quickView_Objects = ["name" , "count"]
content3 = "public enum %s {  case %s , %s  }\n" % (quickViewList_cases,quickView_Objects[0],quickView_Objects[1])

contactList_cases = "ZDContactListCases"
contactList_Objects = ["email" , "phone"]
content4 = "public enum %s {  case %s , %s  }\n" % (contactList_cases,contactList_Objects[0],contactList_Objects[1])

file1.write(screenContent + "\n" + content1 + "\n" +content2 + "\n" +content3 + "\n" + content4)
file1.close()
