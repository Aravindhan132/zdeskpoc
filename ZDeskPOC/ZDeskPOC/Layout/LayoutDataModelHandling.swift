//
//  LayoutDataModelHandling.swift
//  ZDeskPOC
//
//  Created by Rathish Marthandan T on 09/03/20.
//  Copyright © 2020 Zoho. All rights reserved.
//

import UIKit

public class LayoutDataModelHandling: NSObject {

    public func loadLayoutInDB(_ onComplition: (() -> Void)?) {
        
        let object = JSONParser().readJSONFromFile() //Data from API
        
        object?.layouts.forEach({ (layout) in
            CoreDataHelper.deleteForms(ofType: layout.layoutType, ofTitle: layout.title)
        })
        
        CoreDataHelper.insertLayouts(object) { //DB insert

            let layout = object?.layouts.first
            let formLayout = CoreDataHelper.fetchLayout(ofType: layout?.layoutType, ofTitle: layout?.title)
 
            print(formLayout ?? (Any).self)
            
            let patterns = formLayout?.pattern?.allObjects
            let pattern = patterns?.first as? FormFieldPattern
            print(pattern?.action as Any)
            print(pattern?.fields as Any)

            onComplition?()
        }
        
    }
    
}
