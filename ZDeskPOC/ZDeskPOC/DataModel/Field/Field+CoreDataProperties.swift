//
//  Field+CoreDataProperties.swift
//  
//
//  Created by Rathish Marthandan T on 09/03/20.
//
//

import Foundation
import CoreData


extension Field {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Field> {
        return NSFetchRequest<Field>(entityName: "Field")
    }

    @NSManaged public var fieldBgColor: String?
    @NSManaged public var fieldColor: String?
    @NSManaged public var fieldKey: String?
    @NSManaged public var fieldName: String?
    @NSManaged public var fieldValue: String?
    @NSManaged public var fieldOrder: Int32

}
