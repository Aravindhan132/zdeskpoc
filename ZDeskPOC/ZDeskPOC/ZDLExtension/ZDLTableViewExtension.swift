//
//  ZDLTableViewExtension.swift
//  ZDesklayoutComponents
//
//  Created by aravind-zt336 on 23/02/20.
//  Copyright © 2020 Zoho. All rights reserved.
//

import Foundation
import UIKit

extension UITableView{
    func loadMoreDataLoader(){
        self.tableFooterView = self.getFooterView()
    }
    
    func getFooterView() -> UIView{
        let footerView = UIView(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: self.bounds.width, height: 50)))
        
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.style = UIActivityIndicatorView.Style.gray
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        activityIndicator.center = footerView.center
        activityIndicator.startAnimating()
        footerView.addSubview(activityIndicator)
        
        return footerView
    }
    
    func cellLineConfigure(cell:UITableViewCell){
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
        if cell.responds(to: #selector(setter: UITableViewCell.layoutMargins)) {
            cell.layoutMargins = UIEdgeInsets.zero
        }
        if cell.responds(to: #selector(setter: UITableViewCell.preservesSuperviewLayoutMargins)) {
            cell.preservesSuperviewLayoutMargins = false
        }
    }
}


extension UIView {
    func transformAnimation(duration: TimeInterval, bounceLength: CGFloat, onCompletion: @escaping () -> Void) {

           UIView.transition(with: self, duration: duration, options: UIView.AnimationOptions.curveEaseInOut, animations: {
               self.transform = CGAffineTransform(scaleX: bounceLength, y: bounceLength)
           }) { (finished) in
               UIView.animate(withDuration: duration, animations: {
                   self.transform = CGAffineTransform.identity
               }) { (_) in
                   onCompletion() // This makes sense
                   // this make the user feels like action started once got pressed
               }
           }
       }
}
public extension UIView {
    static func loadFromXib<T>(withOwner: Any? = nil, options: [UINib.OptionsKey: Any]? = nil) -> T where T: UIView {
        let bundle = Bundle(for: self)
        let nib = UINib(nibName: "\(self)", bundle: bundle)

        guard let view = nib.instantiate(withOwner: withOwner, options: options).first as? T else {
            fatalError("Could not load view from nib file.")
        }
        return view
    }
}
