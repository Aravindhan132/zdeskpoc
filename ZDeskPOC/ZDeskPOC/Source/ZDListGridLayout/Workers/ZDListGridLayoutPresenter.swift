//
//  ZDListGridLayoutPresenter.swift
//  ZDeskPOC
//
//  Created by aravind-zt336 on 09/03/20.
//  Copyright (c) 2020 Zoho. All rights reserved.
//  Modify By:  * Aravindhan
//              * aravindhan.natarajan@zohocorp.com

import UIKit

protocol IZDListGridLayoutPresenter: class {
	// do someting...
    func presentLayout(form: FormView?)
}

class ZDListGridLayoutPresenter: IZDListGridLayoutPresenter {	
	weak var view: IZDListGridLayoutViewController?
	
	init(view: IZDListGridLayoutViewController?) {
		self.view = view
	}
    
    func presentLayout(form: FormView?) {
        self.view?.presentLayout(form: form)
    }
}
