//
//  Action+CoreDataProperties.swift
//  
//
//  Created by Rathish Marthandan T on 12/03/20.
//
//

import Foundation
import CoreData


extension Action {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Action> {
        return NSFetchRequest<Action>(entityName: "Action")
    }

    @NSManaged public var uiAction: String?
    @NSManaged public var perform: String?
    @NSManaged public var haveCallBack: Bool
    @NSManaged public var navigation: NSSet?

}

// MARK: Generated accessors for navigation
extension Action {

    @objc(addNavigationObject:)
    @NSManaged public func addToNavigation(_ value: Navigation)

    @objc(removeNavigationObject:)
    @NSManaged public func removeFromNavigation(_ value: Navigation)

    @objc(addNavigation:)
    @NSManaged public func addToNavigation(_ values: NSSet)

    @objc(removeNavigation:)
    @NSManaged public func removeFromNavigation(_ values: NSSet)

}
