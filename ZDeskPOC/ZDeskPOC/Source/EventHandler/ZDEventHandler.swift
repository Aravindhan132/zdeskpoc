//
//  ZDEventHandler.swift
//  ZDeskPOC
//
//  Created by aravind-zt336 on 24/03/20.
//  Copyright © 2020 Zoho. All rights reserved.
//

import Foundation
import UIKit

protocol ZDListEventsProtocol: UIViewController {
    func preActionCompletion(for action: Action? , selectedIndex: IndexPath , screenTitle: String , haveCallback: Bool , navigationModel: Navigation?)
    func preparePostActionCompletion(for action: Action? , selectedIndex: IndexPath , screenTitle: String , parentModel: Any?, navigationModel: Navigation?)
}


class ZDEventHandler: NSObject {
    
    internal var eventProtocol: ZDListEventsProtocol? = nil
    internal var listDelegate: ZDListLayoutDelegate? = nil
    
    
    func triggerAction(for action: Action? , selectedIndex: IndexPath , swipeTitle: String , sourceIndex: IndexPath? , screenTitle: String) -> Void {
        
        self.listDelegate?.preActionDelegate(for: swipeTitle, sourceIndex: sourceIndex, selectedIndex: selectedIndex, title: screenTitle, { (isCompleted) in
            
            guard let navigation = action?.navigation ,
                let navigationModel = (Array<Any>(navigation) as? [Navigation])?.first else {
                    self.preparePreActionCompletion(with: nil, action: action, selectedIndex: selectedIndex, screenTitle: screenTitle)
                    return
            }
            
            
            self.preparePreActionCompletion(with: navigationModel, action: action, selectedIndex: selectedIndex, screenTitle: screenTitle)
            
            self.preparePostAction(action: action, navigationModel: navigationModel, actionTitle: swipeTitle, screenTitle: screenTitle, sourceIndex: sourceIndex, selectedIndex: selectedIndex)
            
        })
        
    }
    
}

extension ZDEventHandler {
    func preparePreActionCompletion(with model: Navigation? , action: Action? , selectedIndex: IndexPath , screenTitle: String) {
        self.eventProtocol?.preActionCompletion(for: action,
                                                selectedIndex: selectedIndex,
                                                screenTitle:  screenTitle ,
                                                haveCallback: action?.haveCallBack ?? false,
                                                navigationModel: nil)
    }
    
    func preparePostActionCompletion(with model: Navigation? , action: Action? , selectedIndex: IndexPath , screenTitle: String , parentModel: Any? ) {
        self.eventProtocol?.preparePostActionCompletion(for: action,
                                                        selectedIndex: selectedIndex,
                                                        screenTitle:  screenTitle,
                                                        parentModel: parentModel,
                                                        navigationModel: model)
    }
    
    func navigateOtherController(navigationModel: Navigation , parentData: Any? , _ onCompletion: (() -> Void)? = nil) {
        let isGrid = CoreDataHelper.fetchLayouts()?.filter({$0.title == navigationModel.navigateTo ?? ""}).first?.isGrid ?? false
        ZDLayoutNavigator.moveToNextController(   form: navigationModel,
                                                  parentData: parentData,
                                                  view: self.eventProtocol,
                                                  isGrid: isGrid) {
                                                    onCompletion?()
        }
    }
    
}

extension ZDEventHandler {
    
    func preparePostAction(action: Action?, navigationModel: Navigation , actionTitle: String , screenTitle: String , sourceIndex: IndexPath? , selectedIndex: IndexPath) {
        if !(action?.haveCallBack ?? true) {
            self.preparePostActionWithOutParentModel(action: action,
                                                     navigationModel: navigationModel,
                                                     actionTitle: actionTitle,
                                                     screenTitle: screenTitle,
                                                     sourceIndex: sourceIndex,
                                                     selectedIndex: selectedIndex)
        } else {
            self.preparePostActionWithParentModel(action: action,
                                                  navigationModel: navigationModel,
                                                  actionTitle: actionTitle,
                                                  screenTitle: screenTitle,
                                                  sourceIndex: sourceIndex,
                                                  selectedIndex: selectedIndex)
        }
    }
    
    func preparePostActionWithOutParentModel(action: Action?, navigationModel: Navigation , actionTitle: String , screenTitle: String , sourceIndex: IndexPath? , selectedIndex: IndexPath) {
        self.navigateOtherController(navigationModel: navigationModel, parentData: nil) {
            
            self.listDelegate?.postActionDelegate(for: actionTitle,
                                                  title: screenTitle,
                                                  sourceIndex: sourceIndex,
                                                  selectedIndex: selectedIndex, { (parentModel) in
                                                    self.preparePostActionCompletion(with: navigationModel,
                                                                                     action: action,
                                                                                     selectedIndex: selectedIndex,
                                                                                     screenTitle: screenTitle,
                                                                                     parentModel: parentModel)
            })
        }
    }
    
    func preparePostActionWithParentModel(action: Action?, navigationModel: Navigation , actionTitle: String , screenTitle: String , sourceIndex: IndexPath? , selectedIndex: IndexPath) {
        self.listDelegate?.postActionDelegate(for: actionTitle,
                                              title: screenTitle,
                                              sourceIndex: sourceIndex,
                                              selectedIndex: selectedIndex, { (parentModel) in
                                                self.navigateOtherController(navigationModel: navigationModel, parentData: parentModel) {
                                                    self.preparePostActionCompletion(with: navigationModel,
                                                                                     action: action,
                                                                                     selectedIndex: selectedIndex,
                                                                                     screenTitle: screenTitle,
                                                                                     parentModel: parentModel)
                                                }
        })
    }
}
