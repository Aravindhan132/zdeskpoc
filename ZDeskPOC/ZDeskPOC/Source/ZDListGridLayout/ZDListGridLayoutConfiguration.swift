//
//  ZDListGridLayoutConfiguration.swift
//  ZDeskPOC
//
//  Created by aravind-zt336 on 09/03/20.
//  Copyright (c) 2020 Zoho. All rights reserved.
//  Modify By:  * Aravindhan
//              * aravindhan.natarajan@zohocorp.com

import Foundation
import UIKit

public class ZDListGridLayoutConfiguration {
    public static func setup(parameters: [String: Any] = [:]) -> ZDListGridLayoutViewController {
        let controller = getController("ZDListGrid", "ZDListGridLayoutViewController") as! ZDListGridLayoutViewController
        let router = ZDListGridLayoutRouter(view: controller)
        let presenter = ZDListGridLayoutPresenter(view: controller)
        let manager = ZDListGridLayoutManager()
        let interactor = ZDListGridLayoutInteractor(presenter: presenter, manager: manager)
        
        controller.interactor = interactor
        controller.router = router
        interactor.parameters = parameters
        return controller
    }
}
