//
//  CoreDataHelper.swift
//  ZDeskPOC
//
//  Created by Rathish Marthandan T on 09/03/20.
//  Copyright © 2020 Zoho. All rights reserved.
//

import UIKit
import CoreData

public class CoreDataHelper: NSObject {
    
    static let FORM_ENTITY = "FormView"
    static let NAVIGATIONITEM_ENTITY = "NavigationItem"
    static let FORMFIELD_PATTERN_ENTITY = "FormFieldPattern"
    static let ACTION_ENTITY = "Action"
    static let FIELD_ENTITY = "Field"
    static let NAVIGATION_ENTITY = "Navigation"

    static func newFormEntityObject(withContext context : NSManagedObjectContext = CoreDataHandler.sharedInstance.getPrivateContext()) -> FormView? {
        let formEntity = NSEntityDescription.insertNewObject(forEntityName: CoreDataHelper.FORM_ENTITY, into: context) as? FormView
        return formEntity
    }

    static func newNavigationItemEntityObject(withContext context : NSManagedObjectContext = CoreDataHandler.sharedInstance.getPrivateContext()) -> NavigationItem? {
        let navigationItemEntity = NSEntityDescription.insertNewObject(forEntityName: CoreDataHelper.NAVIGATIONITEM_ENTITY, into: context) as? NavigationItem
        return navigationItemEntity
    }

    static func newFormFieldPatternEntityObject(withContext context : NSManagedObjectContext = CoreDataHandler.sharedInstance.getPrivateContext()) -> FormFieldPattern? {
        let formFieldPatternEntity = NSEntityDescription.insertNewObject(forEntityName: CoreDataHelper.FORMFIELD_PATTERN_ENTITY, into: context) as? FormFieldPattern
        return formFieldPatternEntity
    }

    static func newActionEntityObject(withContext context : NSManagedObjectContext = CoreDataHandler.sharedInstance.getPrivateContext()) -> Action? {
        let actionEntity = NSEntityDescription.insertNewObject(forEntityName: CoreDataHelper.ACTION_ENTITY, into: context) as? Action
        return actionEntity
    }
    
    static func newFieldEntityObject(withContext context : NSManagedObjectContext = CoreDataHandler.sharedInstance.getPrivateContext()) -> Field? {
        let fieldEntity = NSEntityDescription.insertNewObject(forEntityName: CoreDataHelper.FIELD_ENTITY, into: context) as? Field
        return fieldEntity
    }
    
    static func newNavigationEntityObject(withContext context : NSManagedObjectContext = CoreDataHandler.sharedInstance.getPrivateContext()) -> Navigation? {
        let navigationEntity = NSEntityDescription.insertNewObject(forEntityName: CoreDataHelper.NAVIGATION_ENTITY, into: context) as? Navigation
        return navigationEntity
    }

    //================= ****************** ================//

    static func insertLayouts(_ layouts: LayoutCodable?, onComplition:(() -> ())? = nil) {
        
        CoreDataHandler.sharedInstance.persistentContainer.performBackgroundTask { (context) in
            
            layouts?.layouts.forEach({ (layout) in
                
                let formView = CoreDataHelper.newFormEntityObject(withContext: context)
                formView?.isGrid = layout.isGrid
                formView?.layoutType = layout.layoutType
                formView?.title = layout.title
                
                layout.navigationBarItems.forEach { (navBarItem) in
                    guard let formContext = formView?.managedObjectContext, let navigationItem = CoreDataHelper.newNavigationItemEntityObject(withContext: formContext) else { return }
                    navigationItem.icon = navBarItem.icon
                    navigationItem.name = navBarItem.name
                    navigationItem.navigateTo = navBarItem.navigateTo
                    formView?.addToNavigationItems(navigationItem)
                }
                
                guard let formContext = formView?.managedObjectContext, let formFieldPattern = CoreDataHelper.newFormFieldPatternEntityObject(withContext: formContext) else { return }
                formFieldPattern.fieldPatternName = layout.zpattern.patternType
                
                
                guard let formFieldContext = formFieldPattern.managedObjectContext else { return }
                let zActions = layout.zpattern.zactions
                zActions?.forEach { (zaction) in
                    guard let action = CoreDataHelper.newActionEntityObject(withContext: formFieldContext) else { return }
                    action.uiAction = zaction.uiAction
                    action.perform = zaction.perform
                    action.haveCallBack = zaction.haveCallBack
                    
                    guard let znavigation = zaction.zNavigation,
                        let actionContext = action.managedObjectContext,
                        let navigation = CoreDataHelper.newNavigationEntityObject(withContext: actionContext) else {
                        formFieldPattern.addToAction(action)
                        return
                    }
                    navigation.navigateTo = znavigation.navigateTo
                    navigation.transition = znavigation.transition
                    action.addToNavigation(navigation)
                    formFieldPattern.addToAction(action)
                }
                
                let zFields = layout.zpattern.fieldMapping
                zFields.forEach { (zfield) in
                    guard let field = CoreDataHelper.newFieldEntityObject(withContext: formFieldContext) else { return }
                    field.fieldOrder = Int32(zfield.fieldOrder) ?? 0
                    field.fieldName = zfield.fieldName
                    field.fieldBgColor = zfield.fieldBgColor.rawValue
                    field.fieldColor = zfield.fieldColor.rawValue
                    field.fieldKey = zfield.fieldKey.rawValue
                    formFieldPattern.addToFields(field)
                }
                
                formView?.addToPattern(formFieldPattern)
            })
            
            context.saveContext()
            onComplition?()
        }
        
    }
    
    static func fetchLayouts(of context: NSManagedObjectContext = CoreDataHandler.sharedInstance.getMainContext()) -> [FormView]? {
        let fetchRequest: NSFetchRequest<FormView> = FormView.fetchRequest()
//        fetchRequest.predicate = NSPredicate(format: "layoutType == %@ AND title == %@", type ?? "", title ?? "")
        guard let result = try? context.fetch(fetchRequest), result.count > 0 else { return nil }
        return result
    }
    
    
    static func fetchLayout(ofType type: String?, ofTitle title:String?, of context: NSManagedObjectContext = CoreDataHandler.sharedInstance.getMainContext()) -> FormView? {
        let fetchRequest: NSFetchRequest<FormView> = FormView.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "layoutType == %@ AND title == %@", type ?? "", title ?? "")
        guard let result = try? context.fetch(fetchRequest), result.count > 0 else { return nil }
        return result.first
    }
    
    static func deleteForms(ofType type: String?, ofTitle title:String?, of context: NSManagedObjectContext = CoreDataHandler.sharedInstance.getMainContext(), onComplition:(() -> ())? = nil) {
        
        let fetchRequest: NSFetchRequest<FormView> = FormView.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "layoutType == %@ AND title == %@", type ?? "", title ?? "")
        let objects = try? context.fetch(fetchRequest)
        objects?.forEach({ (form) in
            context.delete(form)
        })
        context.saveContext()
    }
    
}
