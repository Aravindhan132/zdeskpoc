//
//  ZDeskLayoutSDK.swift
//  ZDeskPOC
//
//  Created by aravind-zt336 on 10/03/20.
//  Copyright © 2020 Zoho. All rights reserved.
//

import Foundation
import UIKit

public enum Layouts {
    case list
}

struct InitializationConstant {
    static var rootController: UIViewController? = nil
}

public class ZDeskLayoutSDK {
    var listDataSource: ZDListLayoutDataSource? = nil
    var listDelegate: ZDListLayoutDelegate? = nil
    
    var screenName: String = ""
    
    @discardableResult public init(controller: UIViewController?) {
        guard controller != nil else {return}
        InitializationConstant.rootController  = controller
        fetchLayoutAPI()
    }
    
    internal func fetchLayoutAPI() -> Void {
        LayoutDataModelHandling().loadLayoutInDB {
//            let forms = CoreDataHelper.fetchLayouts()
//            let layout = forms?[0].layoutType
            let isGridLayout = false
            self.screenName = "DepartmentListView"
            self.prepareLayout(type: .list , isGrid: isGridLayout)
        }
    }
    
    fileprivate func prepareLayout(type: Layouts , isGrid: Bool) {
        DispatchQueue.main.async {
            switch type {
            case .list:
                guard let window = UIApplication.shared.windows.first else {return}
                window.rootViewController  = UINavigationController(rootViewController: isGrid ? self.preapreGridFormLayout() : self.prepareFormLayout())
            }
        }
        
    }
    
    
    @discardableResult internal func prepareFormLayout() -> ZDListLayoutViewController{
        let controller = ZDListLayoutConfiguration.setup()
        controller.screenName = self.screenName
        if let dataSourcedController = InitializationConstant.rootController as? ZDListLayoutDataSource{
            controller.listDataSource = dataSourcedController
        }
        if let delegatedController = InitializationConstant.rootController as? ZDListLayoutDelegate{
            controller.listDelegate = delegatedController
        }
        return controller
    }
    
    @discardableResult internal func preapreGridFormLayout() -> ZDListGridLayoutViewController {
        let controller = ZDListGridLayoutConfiguration.setup()
        controller.screenName = self.screenName
        if let dataSourcedController = InitializationConstant.rootController as? ZDListLayoutDataSource{
            controller.listDataSource = dataSourcedController
        }
        if let delegatedController = InitializationConstant.rootController as? ZDListLayoutDelegate{
            controller.listDelegate = delegatedController
        }
        return controller
    }
    
    
}
