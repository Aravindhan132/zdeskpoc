//
//  NavigationItem+CoreDataProperties.swift
//  
//
//  Created by Rathish Marthandan T on 09/03/20.
//
//

import Foundation
import CoreData


extension NavigationItem {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<NavigationItem> {
        return NSFetchRequest<NavigationItem>(entityName: "NavigationItem")
    }

    @NSManaged public var icon: String?
    @NSManaged public var name: String?
    @NSManaged public var navigateTo: String?

}
