//
//  ZDListPattern2.swift
//  ZDeskPOC
//
//  Created by aravind-zt336 on 17/03/20.
//  Copyright © 2020 Zoho. All rights reserved.
//

import UIKit

class ZDListPattern2: UITableViewCell {

    
    @IBOutlet var label1: UILabel!
    @IBOutlet var label2: UILabel!
    @IBOutlet var imageview: UIImageView!
    
    
    var labelArray: [UILabel] = []
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
        imageview.layer.cornerRadius = imageview.frame.height / 2
        imageview.backgroundColor = .lightGray
        imageview.alpha = 0
        self.labelArray = [label1 , label2]
        
        labelArray.forEach({
            $0.text = ""
            $0.backgroundColor = .clear
        })
        self.backgroundColor = .clear
    }

   
    internal func loadData(for values: [ZDListDataItem]?) {
        guard let nonNilValue = values else {return}
        imageview.alpha = 1
        labelArray.enumerated().forEach { (index , eachLabel) in
            if index < nonNilValue.count {
                eachLabel.text = nonNilValue[index].value ?? ""
            }
        }
        
    }
    
}
