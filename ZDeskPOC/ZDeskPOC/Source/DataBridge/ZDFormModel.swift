//
//  ZDFormModel.swift
//  ZDeskPOC
//
//  Created by Rathish Marthandan T on 10/03/20.
//  Copyright © 2020 Zoho. All rights reserved.
//

import UIKit

public typealias onItemCompletion = ((_ items: [ZDListDataItem]?) -> Void)?
public typealias onDataListItemCompletion = ((_ dataitems: [[ZDListDataItem]]?) -> Void)?
public protocol ZDLayoutDataSource: ZDListLayoutDataSource {}


//MARK:- DataBridge Delegates
public protocol ZDListLayoutDataSource: class {
    func prepareInitialDataSource(layout: Layouts , screenTitle: String , parentDataModel: Any? , _ onCompletion: (() -> Void)?)
    func getTotalItems(layout: Layouts , screenTitle: String) -> Int
    func listItem(layout: Layouts , screenTitle: String , atIndexPath indexPath: IndexPath, fromKeys keys: [ZDListDataItem]?, _ onCompletion: onItemCompletion)
    func getLoadMoreDataSource(layout: Layouts , screenTitle: String , fromIndexPath indexPath: IndexPath, fromValue: Int, _ onCompletion: (() -> Void)?)
}


//MARK:- Actions Delegates
public protocol ZDListLayoutDelegate {
    func preActionDelegate(for action: String?, sourceIndex: IndexPath?, selectedIndex: IndexPath ,title: String? , _ onCompletion: ((Bool) -> ())?)
    func postActionDelegate(for action: String , title: String? , sourceIndex: IndexPath?, selectedIndex: IndexPath , _ onCompletion: ((Any?) -> ())?)
}

 
protocol ZDListProtocol: class {
    func getParentIndex() -> IndexPath?
    func refreshPage()
}

//MARK:- DataBridge Model
public class ZDListDataItem {
    public var key: String? = nil
    public var value: String? = nil
    
    public init(key: String?, value: String?) {
        self.key = key
        self.value = value
    }
}

class ZDFormModel: NSObject {
    
    static func generateDataItem(usingKeys keys: [String]?) -> [ZDListDataItem]? {
        
        var dataItems = [ZDListDataItem]()

        keys?.forEach({ (key) in
            let dataItem = ZDListDataItem.init(key: key, value: nil)
            dataItems.append(dataItem)
        })
        
        return dataItems
    }
    
}


internal enum listpatterns: String {
    case pattern1 = "pattern1"
    case pattern2 = "pattern2"
    case pattern3 = "pattern3"
    case pattern4 = "pattern4"
    case pattern5 = "pattern5"
}

 enum appletType: String {
    case list = "Form"
}

internal enum fieldActions: String {
    case leftswipe = "leftswipe"
    case rightswift = "rightswipe"
    case tap = "Tap"
}
