//
//  ZDListLayoutPresenter.swift
//  ZDeskPOC
//
//  Created by aravind-zt336 on 09/03/20.
//  Copyright (c) 2020 Zoho. All rights reserved.
//  Modify By:  * Aravindhan
//              * aravindhan.natarajan@zohocorp.com

import UIKit

protocol IZDListLayoutPresenter: class {
	// do someting...
    func presentLayout(form: FormView? , fields: [Field] , patternType: String?)
}

class ZDListLayoutPresenter: IZDListLayoutPresenter {
    
	weak var view: IZDListLayoutViewController?
	
	init(view: IZDListLayoutViewController?) {
		self.view = view
	}
    
    func presentLayout(form: FormView?, fields: [Field] , patternType: String?) {
        view?.presentLayout(form: form, fields: fields , patternType: patternType)
    }
    
}
