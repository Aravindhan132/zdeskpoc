//
//  ZDListLayoutInteractor.swift
//  ZDeskPOC
//
//  Created by aravind-zt336 on 09/03/20.
//  Copyright (c) 2020 Zoho. All rights reserved.
//  Modify By:  * Aravindhan
//              * aravindhan.natarajan@zohocorp.com

import UIKit

protocol IZDListLayoutInteractor: class {
    var parameters: [String: Any]? { get set }
    
    func getListLayout(screenTitle: String?)
}


class ZDListLayoutInteractor: IZDListLayoutInteractor {
    var presenter: IZDListLayoutPresenter?
    var manager: IZDListLayoutManager?
    var parameters: [String: Any]?

    init(presenter: IZDListLayoutPresenter, manager: IZDListLayoutManager) {
    	self.presenter = presenter
    	self.manager = manager
    }
    
    func getListLayout(screenTitle: String?) {
       
        LayoutDataModelHandling().loadLayoutInDB {
            let form = CoreDataHelper.fetchLayout(ofType: appletType.list.rawValue, ofTitle: (screenTitle ?? "TicketListView"))
            guard let nonNilpattern = ((form?.pattern? .allObjects)?.first as? FormFieldPattern)?.fields else {return}
            
            
            var fields = Array<Any>(nonNilpattern) as? [Field]
            fields = fields?.sorted { (field1, field2) -> Bool in
                field1.fieldOrder < field2.fieldOrder
            }
            guard let nonNilFields = fields else {return}
            self.presenter?.presentLayout(form: form, fields: nonNilFields, patternType: ((form?.pattern? .allObjects)?.first as? FormFieldPattern)?.fieldPatternName)
        }
    }
     
}

extension ZDListLayoutViewController {
    func loadingNextTableViewContent(_ indexPath: IndexPath , _ tableView: UITableView) {
        let count = self.listDataSource?.getTotalItems(layout: .list, screenTitle: screenName) ?? .zero
        
        if indexPath.row == count-1 && count >= 20 - 1 && isLoadMoreEnabled{
            self.listTableView.loadMoreDataLoader()
            
            self.listDataSource?.getLoadMoreDataSource(layout: .list, screenTitle: screenName,fromIndexPath: indexPath, fromValue: count, {
               
                DispatchQueue.main.async {
                    self.isLoadMoreEnabled = (self.listDataSource?.getTotalItems(layout: .list, screenTitle: self.screenName) ?? .zero) != tableView.numberOfRows(inSection: 0)
                    self.listTableView.tableFooterView = nil
                    self.listTableView.reloadData()
                }
            })
        }
    }
}

extension ZDListLayoutViewController {
    func startAnimation() {
        DispatchQueue.main.async {
            self.activityIndicator.startAnimating()
        }
    }
    
    func stopAnimation() {
       DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
        }
    }
    
    internal func prepareCellPattern(for indexpath: IndexPath) -> UITableViewCell {
        self.loadingNextTableViewContent(indexpath, listTableView)

        switch layoutType {
        case listpatterns.pattern1.rawValue:
            return preparePatttern1(listTableView, cellForRowAt: indexpath)
        case listpatterns.pattern2.rawValue:
            return preparePatttern2(listTableView, cellForRowAt: indexpath)
        case listpatterns.pattern3.rawValue:
            return preparePatttern3(listTableView, cellForRowAt: indexpath)
        case listpatterns.pattern4.rawValue:
            return preparePatttern4(listTableView, cellForRowAt: indexpath)
        case listpatterns.pattern5.rawValue:
            return preparePatttern5(listTableView, cellForRowAt: indexpath)
        default:
            return UITableViewCell()
        }
    }
    
    internal func preparePatttern1(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "ZDListPattern1", for: indexPath) as! ZDListPattern1
        self.listDataSource?.listItem(layout: .list, screenTitle: screenName,atIndexPath: indexPath, fromKeys: ZDFormModel.generateDataItem(usingKeys: field_Keys), { (listDataItems) in
            cell.loadData(for: listDataItems)
        })
        
        return cell
    }
    
    internal func preparePatttern2(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ZDListPattern2", for: indexPath) as! ZDListPattern2
        self.listDataSource?.listItem(layout: .list, screenTitle: screenName,atIndexPath: indexPath, fromKeys: ZDFormModel.generateDataItem(usingKeys: field_Keys), { (listDataItems) in
            cell.loadData(for: listDataItems)
        })
        
        return cell
        
    }
    
    internal func preparePatttern3(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ZDListPattern3", for: indexPath) as! ZDListPattern3
        self.listDataSource?.listItem(layout: .list, screenTitle: screenName ,atIndexPath: indexPath, fromKeys: ZDFormModel.generateDataItem(usingKeys: field_Keys), { (listDataItems) in
            cell.loadData(for: listDataItems)
        })
        
        return cell
        
    }
    
    internal func preparePatttern4(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ZDListPattern4", for: indexPath) as! ZDListPattern4
        self.listDataSource?.listItem(layout: .list, screenTitle: screenName ,atIndexPath: indexPath, fromKeys: ZDFormModel.generateDataItem(usingKeys: field_Keys), { (listDataItems) in
            cell.loadData(for: listDataItems)
        })
        
        return cell
        
    }
    
    internal func preparePatttern5(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "ZDListPattern5", for: indexPath) as! ZDListPattern5
        self.listDataSource?.listItem(layout: .list, screenTitle: screenName ,atIndexPath: indexPath, fromKeys: ZDFormModel.generateDataItem(usingKeys: field_Keys), { (listDataItems) in
            cell.loadData(for: listDataItems)
        })
        
        return cell
    }
}
