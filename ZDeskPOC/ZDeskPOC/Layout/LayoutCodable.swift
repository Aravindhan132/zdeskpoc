//
//  LayoutCodable.swift
//  ZDeskPOC
//
//  Created by Rathish Marthandan T on 06/03/20.
//  Copyright © 2020 Zoho. All rights reserved.
//

import UIKit

// MARK: - LayoutCodable
public class LayoutCodable: Codable {
    let layouts: [Layout]

    init(layouts: [Layout]) {
        self.layouts = layouts
    }
}

// MARK: - Layout
class Layout: Codable {
    let id: String
    let isGrid: Bool
    let layoutType, title: String
    let navigationBarItems: [NavigationBarItem]
    let zpattern: Zpattern

    init(id: String, isGrid: Bool, layoutType: String, title: String, navigationBarItems: [NavigationBarItem], zpattern: Zpattern) {
        self.id = id
        self.isGrid = isGrid
        self.layoutType = layoutType
        self.title = title
        self.navigationBarItems = navigationBarItems
        self.zpattern = zpattern
    }
}

// MARK: - NavigationBarItem
class NavigationBarItem: Codable {
    let icon, name, navigateTo: String

    init(icon: String, name: String, navigateTo: String) {
        self.icon = icon
        self.name = name
        self.navigateTo = navigateTo
    }
}

// MARK: - Zpattern
class Zpattern: Codable {
    let patternType: String
    let zactions: [Zaction]?
    let fieldMapping: [FieldMapping]
    let zaction: [Zaction]?

    enum CodingKeys: String, CodingKey {
        case patternType, zactions
        case fieldMapping = "FieldMapping"
        case zaction
    }

    init(patternType: String, zactions: [Zaction]?, fieldMapping: [FieldMapping], zaction: [Zaction]?) {
        self.patternType = patternType
        self.zactions = zactions
        self.fieldMapping = fieldMapping
        self.zaction = zaction
    }
}

// MARK: - FieldMapping
class FieldMapping: Codable {
    let fieldOrder: String
    let fieldColor: FieldColor
    let fieldBgColor: FieldBgColor
    let fieldName: String
    let fieldKey: FieldKey

    enum CodingKeys: String, CodingKey {
        case fieldOrder, fieldColor, fieldBgColor
        case fieldName = "field_name"
        case fieldKey = "field_key"
    }

    init(fieldOrder: String, fieldColor: FieldColor, fieldBgColor: FieldBgColor, fieldName: String, fieldKey: FieldKey) {
        self.fieldOrder = fieldOrder
        self.fieldColor = fieldColor
        self.fieldBgColor = fieldBgColor
        self.fieldName = fieldName
        self.fieldKey = fieldKey
    }
}

enum FieldBgColor: String, Codable {
    case clear = "clear"
    case green = "green"
}

enum FieldColor: String, Codable {
    case black = "black"
}

enum FieldKey: String, Codable {
    case displayLabel = "displayLabel"
}

// MARK: - Zaction
class Zaction: Codable {
    let uiAction, perform: String
    let haveCallBack: Bool
    let zNavigation: ZNavigation?

    init(uiAction: String, perform: String, haveCallBack: Bool, zNavigation: ZNavigation?) {
        self.uiAction = uiAction
        self.perform = perform
        self.haveCallBack = haveCallBack
        self.zNavigation = zNavigation
    }
}

// MARK: - ZNavigation
class ZNavigation: Codable {
    let navigateTo, transition: String

    init(navigateTo: String, transition: String) {
        self.navigateTo = navigateTo
        self.transition = transition
    }

}



public class JSONParser: NSObject {
    
    public func readJSONFromFile() -> LayoutCodable?
    {
        var layout: LayoutCodable?
        if let fileUrl = Bundle(for: type(of: self)).url(forResource: "Layout", withExtension: "json") {
            do {
                // Getting data from JSON file using the file URL
                let data = try Data(contentsOf: fileUrl, options: .mappedIfSafe)
                layout = try? JSONDecoder().decode(LayoutCodable.self, from: data )
            } catch {
                // Handle error here
            }
        }
        
        return layout
    }

}
