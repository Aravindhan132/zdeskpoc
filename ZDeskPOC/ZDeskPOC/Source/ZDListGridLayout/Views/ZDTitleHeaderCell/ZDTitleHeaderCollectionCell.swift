//
//  ZDTitleHeaderCollectionCell.swift
//  DeskDemo
//
//  Created by aravind-zt336 on 14/02/20.
//  Copyright © 2020 Zoho. All rights reserved.
//

import UIKit

class ZDTitleHeaderCollectionCell: UICollectionViewCell {
    
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var layoutSwitch: UISwitch!
    
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    
    var isEnableCallBack: ((_ isEnabled: Bool) -> Void)? = nil
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.applyFontAndTheme()
        self.layoutSwitch.isHidden = true
    }
    
    
    func applyFontAndTheme() {
        self.backgroundColor = .clear
    }
    
    func setTitle(name: String) {
        self.titleLabel.text = name
    }
    
    @IBAction func buttonClicked(_ sender: Any) {
        if layoutSwitch.isOn {
            self.isEnableCallBack?(true)
        } else {
            self.isEnableCallBack?(false)
        }
    }
    
    func startRotation() {
        DispatchQueue.main.async {
            self.activityIndicator.startAnimating()
            self.activityIndicator.isHidden = false
        }
    }
    func stopRotation() {
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
            self.activityIndicator.isHidden = true
        }
    }
}
