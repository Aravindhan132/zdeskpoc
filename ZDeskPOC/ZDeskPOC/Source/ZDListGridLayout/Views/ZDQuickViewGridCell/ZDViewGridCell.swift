//
//  ZDViewGridCell.swift
//  DeskDemo
//
//  Created by aravind-zt336 on 14/02/20.
//  Copyright © 2020 Zoho. All rights reserved.
//

import UIKit

class ZDViewGridCell: UICollectionViewCell {

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var countlabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        applyFontAndTheme()
    }
    
    
    func applyFontAndTheme() -> Void {
        applyCorners()
    }
    
    func applyCorners() -> Void {
        self.layer.borderWidth = 0.5
        self.layer.cornerRadius = 10
        self.layer.borderColor = ZDColorConstants.primaryGreen.cgColor
    }
    
    
    func setTitle(as name: String , secondaryLabel: String) -> Void {
        titleLabel.text = name
        countlabel.text = secondaryLabel
    }

}
