//
//  ZDListGridLayoutRouter.swift
//  ZDeskPOC
//
//  Created by aravind-zt336 on 09/03/20.
//  Copyright (c) 2020 Zoho. All rights reserved.
//  Modify By:  * Aravindhan
//              * aravindhan.natarajan@zohocorp.com

import UIKit

protocol IZDListGridLayoutRouter: class {
	// do someting...
}

class ZDListGridLayoutRouter: IZDListGridLayoutRouter {	
	weak var view: ZDListGridLayoutViewController?
	
	init(view: ZDListGridLayoutViewController?) {
		self.view = view
	}
        
    
}
