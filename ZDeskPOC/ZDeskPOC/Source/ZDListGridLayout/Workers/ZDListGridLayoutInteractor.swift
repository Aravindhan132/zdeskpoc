//
//  ZDListGridLayoutInteractor.swift
//  ZDeskPOC
//
//  Created by aravind-zt336 on 09/03/20.
//  Copyright (c) 2020 Zoho. All rights reserved.
//  Modify By:  * Aravindhan
//              * aravindhan.natarajan@zohocorp.com

import UIKit

protocol IZDListGridLayoutInteractor: class {
	var parameters: [String: Any]? { get set }
   func getListLayout(screenTitle: String?)
}


class ZDListGridLayoutInteractor: IZDListGridLayoutInteractor {
    var presenter: IZDListGridLayoutPresenter?
    var manager: IZDListGridLayoutManager?
    var parameters: [String: Any]?

    init(presenter: IZDListGridLayoutPresenter, manager: IZDListGridLayoutManager) {
    	self.presenter = presenter
    	self.manager = manager
    }
    
    func getListLayout(screenTitle: String?) {
       LayoutDataModelHandling().loadLayoutInDB {
        let result = CoreDataHelper.fetchLayout(ofType: appletType.list.rawValue, ofTitle: (screenTitle ?? "TicketListView"))
            self.presenter?.presentLayout(form: result)
        }
    }

}
